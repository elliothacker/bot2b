import Markup from 'node-vk-bot-api/lib/markup.js';

const { keyboard, button } = Markup;
export default ({ bot }) => {
  bot.command('💼 Мой бизнес', ctx => {
    ctx.reply(
      'Получить ЭЦП, узнать о налоговых каникулах или зарегистрироваться самозанятнымт\nНеобходима интеграция с госуслугами',
      null,
      keyboard(
        [
          'Проверка ЮГРИЛ',
          'Регистрация самозанятости',
          'Добавить свою компанию',
          button('Назад', 'positive')
        ],
        { columns: 1 }
      )
    );
  });
  bot.command(['Регистрация самозанятости', 'Проверка ЮГРИЛ'], ctx =>
    ctx.reply('Функция появится в будущем, когда будет возможность')
  );
  bot.command('Добавить свою компанию', ctx => {
    ctx.scene.enter('add');
  });
};
