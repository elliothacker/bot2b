import Markup from 'node-vk-bot-api/lib/markup.js';

const { keyboard, button } = Markup;
export default ({ bot, vk, db }) => {
  bot.command('👨 Поиск', ctx => {
    ctx.reply(
      'Так, это самый настоящий лифт. Найти инвестора или получить миллион рублей грантом стало проще.',
      'photo-185820777_457239043',
      keyboard(['👨 Найти партнёров', '💸 Гранты', 'Назад'], { columns: 1 })
    );
  });

  bot.command('👨 Найти партнёров', ctx =>
    ctx.reply(
      'Смотрите, мы будем подбирать конкретно для человека возможного инвестора',
      null,
      keyboard(['Назад'])
    )
  );
  bot.command('💸 Гранты', ctx =>
    ctx.reply(
      'Мы думает, вам стоит обратить внимание на этот грант: http://grant.myrosmol.ru/',
      null,
      keyboard(['Назад'])
    )
  );
};
