import Markup from 'node-vk-bot-api/lib/markup.js';

const { keyboard } = Markup;
export default ({ bot, kb, db }) => {
  bot.command('начать', ctx => {
    db.collection('users').findOne(
      { vkID: ctx.message.from_id },
      (err, user) => {
        if (!user) {
          ctx.reply(
            'Добрый день. Это бот агентства стратегическх инициатив для начинающий предпринимателей. Мы обучаем, помогаем и продвигаем молодых предпринимателей. ',
            'article-185820777_22654',
            kb
          );
          db.collection('users').insertOne({ vkID: ctx.message.from_id });
        }
      }
    );
  });
};
