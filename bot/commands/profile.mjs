import Markup from 'node-vk-bot-api/lib/markup.js';

const { keyboard, button } = Markup;
export default ({ bot, vk }) => {
  bot.command('👥 Профиль', ctx => {
    vk.api.users.get({ user_ids: ctx.message.from_id }).then(([user]) => {
      ctx.reply(
        `${user.first_name} ${user.last_name}, скорее всего вы начинающий бизнесмен.`,
        null,
        keyboard(
          [
            'Разрешить видеть меня',
            button('Выйти', 'negative'),
            button('Назад', 'positive')
          ],
          { columns: 1 }
        )
      );
    });
  });
  bot.command('Разрешить видеть меня', ctx => {
    ctx.scene.enter('networking');
  });
  bot.command('выйти', ctx => {
    ctx.scene.enter('exit');
  });
};
