import Markup from 'node-vk-bot-api/lib/markup.js';

const { keyboard, button } = Markup;
export default ({ bot, db }) => {
  bot.command('📚 Обучение', ctx => {
    db.collection('users').findOne(
      { vkID: ctx.message.from_id },
      (err, user) => {
        // prettier-ignore
        ctx.reply(
          `Посмотрим, у вас ${user.points ? `${user.points} 🌟`: '0🌟. Вы пока не прошли ни одного урока, 0'
          } \nВремя начать становиться предпринимателем ✌`,
          null,
          keyboard([button('1 Урок', 'primary'), 'Назад'], {columns: 1 })
        );
      }
    );
  });
  bot.command('1 Урок', ctx => {
    ctx.scene.enter('1les');
  });
};
