import Scene from 'node-vk-bot-api/lib/scene.js';
import Markup from 'node-vk-bot-api/lib/markup.js';

const { keyboard, button } = Markup;
export default ({ bot, db, config, kb }) => {
  const scene = new Scene(
    'networking',
    ctx => {
      ctx.reply(
        'Здесь вы можете добавить себя в систему нетворкинга. Другие люди смогут находить вас.',
        null,
        keyboard(
          [button('Добавить', 'positive'), button('Отмена', 'negative')],
          { colums: 1 }
        )
      );
      ctx.scene.next();
    },
    ctx => {
      if (ctx.message.text === 'Добавить') {
        ctx.reply('Вы вас добавили', null, kb);
        db.collection('users').updateOne(
          { vkID: ctx.message.from_id },
          { $set: { discovered: true } }
        );
      } else {
        ctx.reply('Мы отменили ', null, kb);
      }
    }
  );
  return scene;
};
