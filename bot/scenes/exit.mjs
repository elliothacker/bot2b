import Scene from 'node-vk-bot-api/lib/scene.js';
import Markup from 'node-vk-bot-api/lib/markup.js';

const { keyboard, button } = Markup;
export default ({ db, kb }) => {
  const scene = new Scene(
    'exit',
    ctx => {
      ctx.reply(
        'Вы можете выйти из Bot2B и стереть ваши данные. Учтите, что это действие необратимо',
        null,
        keyboard([
          button('Удалить мои данные', 'negative'),
          button('Отмена', 'positive')
        ])
      );
      ctx.scene.next();
    },
    ctx => {
      if (ctx.message.text === 'Удалить мои данные') {
        ctx.reply(
          'Нам очень жаль, что вы уходите. Все ваши данные удаленны\nКогда захотите вернуться - нажмите начать',
          null,
          keyboard([button('Начать', 'positive')])
        );
        db.collection('users').removeOne({ vkID: ctx.message.from_id });
      } else {
        ctx.reply(
          'Отлично! Хорошо, что вы остались с Bot2B. Вас ждет потрясающий бот, куча фич и многое другое',
          null,
          kb
        );
      }
      ctx.scene.leave();
    }
  );
  return scene;
};
