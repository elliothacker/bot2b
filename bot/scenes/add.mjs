import Scene from 'node-vk-bot-api/lib/scene.js';
import Markup from 'node-vk-bot-api/lib/markup.js';
import request from 'request';

const { keyboard, button } = Markup;
export default ({ db, kb }) => {
  const scene = new Scene(
    'add',
    ctx => {
      ctx.reply(
        'Вы можете добавить одну свою команию',
        null,
        keyboard([button('Далее', 'positive'), button('Назад', 'negative')])
      );
      ctx.scene.next();
    },
    ctx => {
      if (ctx.message.text === 'Далее') {
        ctx.reply(
          'Введите ИНН или название вашей компании: ',
          null,
          keyboard(['Отмена'])
        );
      } else {
        ctx.reply('Отмена', null, db);
        ctx.scene.leave();
        return;
      }
      ctx.scene.next();
    },
    ctx => {
      if (ctx.message.text === 'Отмена') {
        ctx.reply('Отмена', null, kb);
        ctx.scene.leave();
        return;
      }
      const inn = ctx.message.text;
      if (inn.length < 3) {
        ctx.reply('Неверный ИНН. Повторите попытку');
        return;
      }
      request.post(
        {
          url:
            'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/party',
          json: { query: inn },
          headers: {
            Authorization: 'Token f0dc80b82784d0ba15b741385b3ea17356bd9a52'
          }
        },
        (req, res, body) => {
          let { suggestions } = body;
          suggestions = suggestions.slice(0, 10);
          if (suggestions.length === 0) {
            ctx.reply('Ничего не найдено. Попробуйте еще');
          }
          let txt = '';
          let i = 1;
          for (const comp of suggestions) {
            txt += `${i}) ${comp.value}\n`;
            i += 1;
          }
          ctx.reply(
            'Выберите вашу компанию: \n' + txt,
            null,
            keyboard(['Отмена'])
          );
          ctx.session.suggestions = suggestions;
          ctx.scene.next();
        }
      );
    },
    ctx => {
      if (ctx.message.text === 'Отмена') {
        ctx.reply('Отмена', null, kb);
        ctx.scene.leave();
        return;
      }
      let idx = ctx.message.text.trim();
      if (isNaN(idx)) {
        ctx.reply('Неверный номер. повторите', null, kb);
        return;
      }
      idx = Number(idx);
      if (
        !Number.isInteger(idx) ||
        Number > ctx.session.suggestions.length ||
        Number < 1
      ) {
        ctx.reply('Неверный номер. Повторите', null, kb);
        return;
      }
      const comp = ctx.session.suggestions[idx - 1];
      ctx.reply(
        `Ваша компания ${comp.value}. Мы ее добавили. Теперь вы будете получать уведомления если что-то пойдет не так`,
        null,
        kb
      );
      ctx.scene.leave();
    }
  );
  return scene;
};
