import Scene from 'node-vk-bot-api/lib/scene.js';
import Markup from 'node-vk-bot-api/lib/markup.js';

const { keyboard } = Markup;
export default ({ bot, db, config, kb }) => {
  const scene = new Scene(
    '1les',
    ctx => {
      ctx.reply(
        'Первым делом вы должны понимать что собственная компания - это большая ответсвенность и риски',
        null,
        keyboard(['Понимаю'])
      );
      ctx.reply(
        'https://vk.com/@asibot2b-kakie-riski-u-individualnyh-predprinimatelei',
        null,
        keyboard(['Понимаю'])
      );
      ctx.scene.next();
    },
    ctx => {
      ctx.scene.next();
      ctx.reply(
        'Хорошо, что вы это понимаете. Теперь давайте проверим ваши знания\nКакой максимальный срок предусмотрен за незаконую предпринимательскую деятельность?',
        null,
        keyboard(['5 лет', '7 лет', '6 месяцев'])
      );
    },
    ctx => {
      if (ctx.message.text === '5 лет') {
        db.collection('users').updateOne(
          { vkID: ctx.message.from_id },
          { $set: { points: 1 } }
        );
        ctx.reply('Всё правильно! И лучше таким не заниматься. 1🌟');
      } else {
        ctx.reply(
          'Нет! За незаконную предпринимательскую деятельность грозит штраф до 5 лет'
        );
      }
      ctx.reply(
        'Все, на этом демо тест закончен. Теперь вы понимаете какие уроки должны быть в Bot2B',
        null,
        kb
      );
      ctx.scene.leave();
    }
  );
  return scene;
};
