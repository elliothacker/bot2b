import Scene from 'node-vk-bot-api/lib/scene.js';
import Markup from 'node-vk-bot-api/lib/markup.js';

const { keyboard } = Markup;
export default ({ bot, db, config }) => {
  const scene = new Scene(
    'start',
    ctx => {
      ctx.reply(
        'Так, а у вы уже зарегистрировали компанию?',
        null,
        keyboard(['Да', 'Нет'])
      );
      ctx.scene.next();
    },
    ctx => {
      if (ctx.message.text === 'Нет') {
        ctx.reply(
          'Понятно, а ты хочешь зарегистрировать ИП? Бот может помочь.',
          null,
          keyboard(['Да, я бы хотел', 'Пока не надо'])
        );
        ctx.scene.next();
      } else {
        ctx.reply('Working hard on it');
        ctx.scene.leave();
      }
    },
    ctx => {
      if (ctx.message.text === 'Да, я бы хотел') {
        ctx.reply(
          'Сколько тебе лет?',
          null,
          keyboard(['Меньше 16', '16-18 лет', 'Уже есть 18'], { columns: 1 })
        );
        ctx.scene.next();
      } else {
        ctx.reply('Очень жаль');
        ctx.scene.leave();
      }
    },
    ctx => {
      if (ctx.message.text === 'Меньше 16') {
        ctx.reply(
          'Нам жаль, но вы не можете зарегестрировать ИП :(\nПрочитайте нашу статью https://vk.com/@asibusiness-problemy-pri-sozdanii-ip-u-nesovershennoletnih'
        );
      } else if (ctx.message.text === '16-18 лет') {
        ctx.session.age = 16;
        ctx.reply(
          'Замечательно, что вы хотите начать свой бизнес в таком возрасте! Конечно, тут не обошлось и без подводных камней. Почитайть о них вы можете в нашей статье.\nhttps://vk.com/@asibusiness-problemy-pri-sozdanii-ip-u-nesovershennoletnih',
          null,
          keyboard(['Далее'])
        );
      } else {
        ctx.session.age = 18;
        ctx.reply(
          'Мы рады, что у вас есть рвение меняться и менять этот мир.',
          '',
          keyboard(['Далее'])
        );
        ctx.scene.next();
      }
    },
    ctx => {
      ctx.reply(
        'Создание и развитие компании это непростой жизненный этап. Вы должны это понимать и правильно оценивать риски. https://vk.com/@asibot2b-kakie-riski-u-individualnyh-predprinimatelei',
        '',
        keyboard(['Понимаю'])
      );
      ctx.scene.next();
    },
    ctx => {
      ctx.reply(
        'Первое, что вам нужно сделать - Определиться с видом деятельности'
      );
    }
  );
  return scene;
};
