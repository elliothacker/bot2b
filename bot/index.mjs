import Markup from 'node-vk-bot-api/lib/markup.js';
import Vkbot from 'node-vk-bot-api';
import Stage from 'node-vk-bot-api/lib/stage.js';
import Session from 'node-vk-bot-api/lib/session.js';
import vkIO from 'vk-io';

import question from './scenes/question.mjs';
import net from './scenes/networking.mjs';
import exit from './scenes/exit.mjs';
import add from './scenes/add.mjs';

import start from './commands/start.mjs';
import education from './commands/education.mjs';
import profile from './commands/profile.mjs';
import contact from './commands/contact.mjs';
import networking from './commands/networking.mjs';
import services from './commands/services.mjs';

const { VK } = vkIO;
const { keyboard, button } = Markup;
const kb = keyboard(
  [
    '📚 Обучение',
    '👨 Поиск',
    '💼 Мой бизнес',
    button('👥 Профиль'),
    button('🔗 Связаться с нами')
  ],
  { columns: 2 }
);

export default ({ config, db }) => {
  const vk = new VK({
    token: config.vk.bot_token
  });

  const bot = new Vkbot(config.vk.bot_token);
  const session = new Session();
  bot.use(session.middleware());

  const stage = new Stage(
    question({ bot, db, config, kb }),
    exit({ db, kb }),
    net({ bot, db, kb }),
    add({ bot, vk, db, config, kb })
  );
  bot.use(stage.middleware());

  start({ bot, db, config, kb });
  networking({ bot, vk, db });
  education({ bot, db });
  contact({ bot });
  profile({ bot, vk });
  services({ bot });

  bot.command(['Назад', 'Отмена'], ctx =>
    ctx.reply('Возвращаемся в главное меню', null, kb)
  );
  bot.on(ctx => {
    ctx.reply('Такой команды не найдено', null, kb);
  });
  bot.startPolling();
};
