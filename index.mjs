import mongodb from 'mongodb';
import config from './config.json';
import bot from './bot/index.mjs';

const { MongoClient } = mongodb;
const mongodbClient = new MongoClient(config.mongourl, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

mongodbClient.connect(err => {
  const db = mongodbClient.db('hackaton');
  bot({ config, db });
});
